Rails.application.routes.draw do
  root 'static_pages#home'

  # static pages
  get  '/help',    to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'

  # user
  resources :users do
    member do
      get :following, :followers
    end
  end
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'

  # session
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  # activation
  resources :account_activations, only: [:edit]

  # microposts
  resources :microposts, only: [:create, :destroy]

  # relationships
  resources :relationships, only: [:create, :destroy]
end
