require 'test_helper'

class RelationshipTest < ActiveSupport::TestCase
  def setup
    load_fixtures
    @relationship = Relationship.new(follower: users('michael@example.com'), followed: users('duchess@example.gov'))
  end

  test "should be valid" do
    assert @relationship.valid?
  end

  test "should require a follower_id" do
    @relationship.follower_id = nil
    assert_not @relationship.valid?
  end

  test "should require a followed_id" do
    @relationship.followed_id = nil
    assert_not @relationship.valid?
  end
end
