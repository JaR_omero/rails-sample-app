def users_fixtures
  User.collection.drop

  User.create!(
      name: 'Michael Example',
      email: 'michael@example.com',
      password_digest: User.digest('password'),
      admin: true,
      activated: true,
      activated_at: Time.zone.now
  )

  User.create!(
      name: 'Sterling Archer',
      email: 'duchess@example.gov',
      password_digest: User.digest('password'),
      activated: true,
      activated_at: Time.zone.now
  )

  User.create!(
      name: 'Lana Kane',
      email: 'lana@example.com',
      password_digest: User.digest('password'),
      activated: true,
      activated_at: Time.zone.now
  )

  User.create!(
      name: 'Malory Archer',
      email: 'malory@example.com',
      password_digest: User.digest('password'),
      activated: true,
      activated_at: Time.zone.now
  )

  30.times do |n|
    User.create!(
        name: "User #{n}",
        email: "user-#{n}@example.com",
        password_digest: User.digest('password'),
        activated: true,
        activated_at: Time.zone.now
    )
  end
end

def users(email)
  User.where(email: email).first
end