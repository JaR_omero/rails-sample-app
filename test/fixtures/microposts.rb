def microposts_fixtures
  Micropost.collection.drop

  Micropost.create!(
      content: "I just ate an orange!",
      created_at: 10.minutes.ago,
      user: users('michael@example.com')
  )

  Micropost.create!(
      content: "Check out the @tauday site by @mhartl: http://tauday.com",
      created_at: 3.years.ago,
      user: users('michael@example.com')
  )

  Micropost.create!(
      content: "Sad cats are sad: http://youtu.be/PKffm2uI4dk",
      created_at: 2.hours.ago,
      user: users('michael@example.com')
  )

  Micropost.create!(
      content: "Writing a short test",
      created_at: Time.zone.now,
      user: users('michael@example.com')
  )

  Micropost.create!(
      content: "Oh, is that what you want? Because that's how you get ants!",
      created_at: 2.years.ago,
      user: users('duchess@example.gov')
  )

  Micropost.create!(
      content: "Danger zone!",
      created_at: 3.days.ago,
      user: users('duchess@example.gov')
  )

  Micropost.create!(
      content: "I'm sorry. Your words made sense, but your sarcastic tone did not.",
      created_at: 10.minutes.ago,
      user: users('lana@example.com')
  )

  Micropost.create!(
      content: "Dude, this van's, like, rolling probable cause.",
      created_at: 4.hours.ago,
      user: users('lana@example.com')
  )

  30.times do |n|
    Micropost.create!(
        content: Faker::Lorem.sentence(5),
        created_at: 42.days.ago,
        user: users('michael@example.com')
    )
  end
end

def microposts(content)
  Micropost.where(content: content).first
end