require_relative './users'
require_relative './microposts'
require_relative './relationships'

def load_fixtures
  users_fixtures
  microposts_fixtures
  relationships_fixtures
end