def relationships_fixtures
  Relationship.collection.drop

  Relationship.create!(follower: users('michael@example.com'), followed: users('lana@example.com'))
  Relationship.create!(follower: users('michael@example.com'), followed: users('malory@example.com'))
  Relationship.create!(follower: users('lana@example.com'), followed: users('michael@example.com'))
  Relationship.create!(follower: users('duchess@example.gov'), followed: users('michael@example.com'))
end

def relationships(follower_id, followed_id)
  Relationship.where(follower: follower_id, followed: followed_id).first
end