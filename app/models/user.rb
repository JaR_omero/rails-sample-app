class User
  include Mongoid::Document
  include ActiveModel::Validations

  attr_accessor :remember_token, :activation_token, :password, :password_confirmation

  field :name, :type => String
  field :email, :type => String
  field :created_at, :type => DateTime
  field :updated_at, :type => DateTime
  field :password_digest, :type => String
  field :remember_digest, :type => String
  field :admin, :type => Boolean, default: false
  field :activation_digest, :type => String
  field :activated, :type => Boolean, default: false
  field :activated_at, :type => DateTime
  index({ email: 1 }, { unique: true })

  has_many :microposts, dependent: :destroy

  has_many :following, class_name: "Relationship", inverse_of: :follower
  has_many :followers, class_name: "Relationship", inverse_of: :followed

  # has_many :active_relationships, {
  #     class_name: "Relationship",
  #     foreign_key: "follower_id",
  #     dependent: :destroy
  # }
  # has_many :passive_relationships, {
  #     class_name:  "Relationship",
  #     foreign_key: "followed_id",
  #     dependent:   :destroy
  # }
  # has_many :following, through: :active_relationships, source: :followed
  # has_many :followers, through: :passive_relationships, source: :follower

  before_save   :downcase_email
  before_create :create_activation_digest

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  validates :name, {
      presence: true,
      length: { maximum: 50 }
  }

  validates :email, {
      presence: true,
      length: { maximum: 255 },
      format: { with: VALID_EMAIL_REGEX },
      uniqueness: { case_sensitive: false }
  }

  validates :password, {
      presence: true,
      length: { minimum: 6, maximum: ActiveModel::SecurePassword::MAX_PASSWORD_LENGTH_ALLOWED },
      allow_nil: true
  }

  validates_confirmation_of :password, allow_blank: true

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Returns the hash digest of the given string.
  def self.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def self.new_token
    SecureRandom.urlsafe_base64
  end

  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

  # Activates an account.
  def activate
    update_attributes(activated: true, activated_at: Time.zone.now)
  end

  # Sends activation email.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  # Follows a user.
  def follow(other_user)
    following << following.build(followed: other_user)
  end

  # Unfollows a user.
  def unfollow(other_user)
    following.each do |f|
      if f.followed === other_user
        following.delete(f)
        f.destroy
      end
    end
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    !!following.detect do |f| f.followed === other_user end
  end

  def followed?(other_user)
    followers.each do |f|
      if f.follower === other_user
        return true
      end
    end

    false
  end

  # Returns a user's status feed.
  def feed
    in_array = following.map {|user| user.followed.id} << self.id
    Micropost.in(user_id: in_array)
  end

  # https://api.rubyonrails.org/classes/ActiveModel/SecurePassword/ClassMethods.html#method-i-has_secure_password
  def password=(unencrypted_password)
    if unencrypted_password.nil?
      self.password_digest = nil
    elsif !unencrypted_password.empty?
      @password = unencrypted_password
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
      self.password_digest = BCrypt::Password.create(unencrypted_password, cost: cost)
    end
  end

  private

    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end
end
