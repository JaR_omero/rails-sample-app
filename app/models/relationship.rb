class Relationship
  include Mongoid::Document
  include ActiveModel::Validations

  belongs_to :followed, class_name: "User", inverse_of: :followers
  belongs_to :follower, class_name: "User", inverse_of: :following

  # belongs_to :follower, class_name: "User"
  # belongs_to :followed, class_name: "User"

  validates :follower_id, presence: true
  validates :followed_id, presence: true
end
